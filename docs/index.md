# Bioshop

This project was generated using the [Thundr GAE generator][generator-thundr-gae]. 

## Prerequisites

Before you can run this app up you'll need to check you have the following tools:

* Java 7*
* Maven 3
* Node >= 6.11.0
* NPM >= 5.0.3

> *Yes you read that right: **Java 7**. App Engine doesn't _quite_ support Java 8 yet so I suggest you head
> over to David Cai's excellent guide to [Install multiple JDKs on Mac][install-multiple-jdk-on-mac].
> This project includes a `.java-version` dot file which will tell jEnv to use JDK 7 when running 
> which will hopefully make your life a lot easier.

## Getting Started

To develop the application you'll need to run two processes. 1) The GAE dev server which runs your 
server-side code and 2) the Webpack dev server which offers nice things like live updating and
hot module reloading.

1. **Start the App Engine dev server**

  `mvn appengine:devserver`

  You can check the server is running by opening [localhost:8080](http://localhost:8080). 
  
  > Note: the static resources served up here will not be updated unless you run `npm run build` or
  > `mvn install` as the Webpack dev server operates entirely in memory.

2. **In a separate terminal window/tab start the Webpack dev server**

  `npm start`

  The Webpack dev server starts on port 3000. Open [localhost:3000](http://localhost:3000) in your
  browser and you should be good to go.

## Deploying

When you're ready to deploy your application you can:

1. **Package your app for deployment**

  `mvn package`

2. **Deploy to App Engine**

  `mvn appengine:update -P<Environment Name> [-DappVersion=<VersionName>]`

  Where environment name matches a profile in your `pom.xml`. Optionally deploy to a slot using `appVersion` property.

## Frontend code structure

We are using a component-based approach with the front-end code. We have not taken the leap to the latest post-1 Angular or React, 
however we have adopted component based design and have also adopted the recommended style guide (at time of author) with a pinch of 
non-iodised sea salt: https://angular.io/guide/styleguide.

We are splitting things into components and modules, trying to move _shared_ components into a `shared` folder, however we do not create 
separate `xxx.module.ts` and `xxx-routing.module.ts`. The guide is mainly based around Angular 4+, so we needed to adapt it to our needs
while keeping things pragmatic and easy-to-maintain. Component structure makes code easier to maintain in that it keeps all relevant resources
together - something that people on other Angular 1.x projects will know is a pain to navigate.

The aim of this Angular 1.x generated codebase is to keep tooling consistent in environments where the skillset is still Angular 1, whilst also
adopting new build tools, and newer, cleaner design in the front-end. Teams are then in a better position to choose React, Angular 4+, or another
front-end technology without such a big leap on concept.

## Backend code structure

Common components that are helpful for new projects are located beneath the `framework` package. The idea is that we will incorporate these
into project sources to begin with and as they stabilise we will eventually move these to their own shared library.

[install-multiple-jdk-on-mac]: http://davidcai.github.io/blog/posts/install-multiple-jdk-on-mac/
[generator-thundr-gae]: https://github.com/3wks/generator-thundr-gae